import os
from iutilities import *
from cached_property import CPM, cached_property
from insertable_odict import InsertableOrderedDict

if os.name == 'nt':
    from networkmaps import *
    from createprocess import *
