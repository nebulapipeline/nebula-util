import os
import re

from collections import namedtuple


symlinkdPattern = re.compile(
    r'^(?:.*)(?P<stype><SYMLINKD?>|<JUNCTION>)(?:\s+)(?P<name>.*)(?:\s+\[)'
    r'(?P<target>.*)(?:\]\s*)$')

symlinkMapping = namedtuple('symlinkMapping', 'location name target stype')


def normpath(path):
    ''' Convert path to a standardized format '''
    path = os.path.normpath(path)
    path = os.path.normcase(path)
    while path.endswith(os.sep):
        path = path[:-1]
    return path
