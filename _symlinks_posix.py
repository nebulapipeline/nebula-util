import os
import subprocess

from _symlinks_common import normpath, symlinkdPattern, symlinkMapping


def getSymlinks(dirpath):
    ''' get symlink mappings by ctypes method
    :type dirpath: str
    '''
    maps = []
    dirpath = normpath(os.path.realpath(dirpath))

    if not os.path.exists(dirpath):
        raise ValueError("Directory %s does not exists" % dirpath)
    if not os.path.isdir(dirpath):
        raise ValueError("%s is not a directory" % dirpath)

    for name in os.listdir(dirpath):
        path = os.path.join(dirpath, name)
        if os.path.islink(path):
            maps.append(
                symlinkMapping(dirpath, name,
                               normpath(os.readlink(path)), '<SYMLINKD>'))
    return maps
