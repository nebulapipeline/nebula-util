'''
Contains implementation of `cached_property` which turns a method into a cached
or sticky property i.e. the last return value of the function is returned is
cached and returned everytime until the cache is removed or updated
Additionally offers a mechanism for exposing a cache enable version of the
original wrapped function for updating the cache when required.

Also contains a metaclass `CPM` to the class of the method in question and this
will add an uncached version of the method prefixed by `get_*` if such an
attribute does not already exist in the class namespace.

Usage:

::
    import random
    class CacheRandom(object):

        # Use CPM as metaclass to automate addition of uncached_getters i.e.
        # get_* functions
        __metaclass__ = CPM

        # wrap the function 
        cached_property
        def random(self):
            return random.random()
        get_random = random.uncached_getter()

        # OR automatically let the code add an uncached_getter function
        random.add_uncached_getter(locals())


'''


__all__ = ['CPM', 'cached_property']


class cached_property(property):
    '''Cached property is a descriptor to wrap / decorate a get function as a
    property whose subsequent access returns its value from cache until
    deleted, or updated'''

    def __init__(self, fget, doc=None):
        super(cached_property, self).__init__(fget, doc=doc)
        if self.fget is not None:
            self.cache_attr = '_%s' % self.fget.func_name
            self.getter_name = 'get_%s' % self.fget.func_name

    def __get__(self, obj, cls):
        if obj is None:
            return self
        if (not hasattr(obj, self.cache_attr)):
            setattr(obj, self.cache_attr, self.fget(obj))
        return getattr(obj, self.cache_attr)

    def __set__(self, obj, value):
        setattr(obj, self.cache_attr, value)

    def __delete__(self, obj):
        delattr(obj, self.cache_attr)

    def setter(self, fset):
        raise AttributeError('Setter is not customizable in cached_property')

    def deleter(self, fdel):
        raise AttributeError('Deleter is not customizable in cached_property')

    def add_uncached_getter(self, namespace):
        if self.getter_name not in namespace:
            namespace[self.getter_name] = self.uncached_getter()

    def uncached_getter(self):
        '''return a cache-decorated version of the original getter function'''
        def _wrapper(obj, *args, **kwargs):
            setattr(obj, self.cache_attr, self.fget(obj))
            return getattr(obj, self.cache_attr)
        _wrapper.__doc__ = self.fget.__doc__
        return _wrapper


class CPM(type):
    '''Cached Property Metaclass:

    Use this as metaclass to automatically add uncached_getters for all the
    cached_properties of this class. This will avoid overriding any existing
    variables in class namespace

    Usage:

    ::
        import random
        class CachedRandom(object):
            __metaclass__ = CPM

            @cached_property
            def random(self):
                return random.random()

    In the above case a wrapped function get_random will be added automatically
    to the class CachedRandom

    '''
    def __new__(cls, name, bases, namespace):
        for key, value in namespace.items():
            if isinstance(value, cached_property):
                value.add_uncached_getter(namespace)
        return super(CPM, cls).__new__(cls, name, bases, namespace)
