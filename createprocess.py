__all__ = ['CreateProcessWithLogonW', 'waitForChild', 'FormatMessageSystem']

from ctypes import (windll, create_unicode_buffer, sizeof, byref, WinError,
                    c_int, pointer)
from ctypes.wintypes import (HANDLE, DWORD, WORD, LPWSTR, Structure, POINTER,
                             Array, LPCWSTR, BOOL, BYTE)

INVALID_HANDLE_VALUE = -1
CREATE_UNICODE_ENVIRONMENT = 0x00000400

CData = Array.__base__
LPBYTE = POINTER(BYTE)


class PROCESS_INFORMATION(Structure):
    '''http://msdn.microsoft.com/en-us/library/ms684873'''
    _fields_ = [
        ('hProcess', HANDLE),
        ('hThread', HANDLE),
        ('dwProcessId', DWORD),
        ('dwThreadId', DWORD),
    ]


LPPROCESS_INFORMATION = POINTER(PROCESS_INFORMATION)


class STARTUPINFOW(Structure):
    'http://msdn.microsoft.com/en-us/library/ms686331'
    _fields_ = [
        ('cb', DWORD),
        ('lpReserved', LPWSTR),
        ('lpDesktop', LPWSTR),
        ('lpTitle', LPWSTR),
        ('dwX', DWORD),
        ('dwY', DWORD),
        ('dwXSize', DWORD),
        ('dwYSize', DWORD),
        ('dwXCountChars', DWORD),
        ('dwYCountChars', DWORD),
        ('dwFillAttribute', DWORD),
        ('dwFlags', DWORD),
        ('wShowWindow', WORD),
        ('cbReserved2', WORD),
        ('lpReserved2', LPBYTE),
        ('hStdInput', HANDLE),
        ('hStdOutput', HANDLE),
        ('hStdError', HANDLE),
    ]


LPSTARTUPINFOW = POINTER(STARTUPINFOW)

# http://msdn.microsoft.com/en-us/library/ms682431
windll.advapi32.CreateProcessWithLogonW.restype = BOOL
windll.advapi32.CreateProcessWithLogonW.argtypes = [
    LPCWSTR,  # lpUsername
    LPCWSTR,  # lpDomain
    LPCWSTR,  # lpPassword
    DWORD,  # dwLogonFlags
    LPCWSTR,  # lpApplicationName
    LPWSTR,  # lpCommandLine (inout)
    DWORD,  # dwCreationFlags
    LPCWSTR,  # lpEnvironment  (force Unicode)
    LPCWSTR,  # lpCurrentDirectory
    LPSTARTUPINFOW,  # lpStartupInfo
    LPPROCESS_INFORMATION,  # lpProcessInfo (out)
]


def CreateProcessWithLogonW(lpUsername=None,
                            lpDomain=None,
                            lpPassword=None,
                            dwLogonFlags=0,
                            lpApplicationName=None,
                            lpCommandLine=None,
                            dwCreationFlags=0,
                            lpEnvironment=None,
                            lpCurrentDirectory=None,
                            startupInfo=None):
    if (lpCommandLine is not None and not isinstance(lpCommandLine, CData)):
        lpCommandLine = create_unicode_buffer(lpCommandLine)
    dwCreationFlags |= CREATE_UNICODE_ENVIRONMENT
    if startupInfo is None:
        startupInfo = STARTUPINFOW(sizeof(STARTUPINFOW))
    processInformation = PROCESS_INFORMATION(INVALID_HANDLE_VALUE,
                                             INVALID_HANDLE_VALUE)
    success = windll.advapi32.CreateProcessWithLogonW(
        lpUsername, lpDomain, lpPassword, dwLogonFlags, lpApplicationName,
        lpCommandLine, dwCreationFlags, lpEnvironment, lpCurrentDirectory,
        byref(startupInfo), byref(processInformation))
    if not success:
        error = windll.kernel32.GetLastError()
        msg = FormatMessageSystem(error)
        raise Exception(msg)
    return processInformation


def waitForChild(processInformation):
    INFINITE = -1
    exitCode = c_int(0)
    pexitCode = pointer(exitCode)

    windll.kernel32.WaitForSingleObject(processInformation.hProcess, INFINITE)
    ec_success = windll.kernel32.GetExitCodeProcess(
        processInformation.hProcess, pexitCode)

    windll.kernel32.CloseHandle(processInformation.hProcess)
    windll.kernel32.CloseHandle(processInformation.hThread)

    if not ec_success:
        raise Exception(FormatMessageSystem(windll.kernel32.GetLastError()))

    return exitCode


LANG_NEUTRAL = 0x00
SUBLANG_NEUTRAL = 0x00
SUBLANG_DEFAULT = 0x01

LANG_ENGLISH = 0x09
SUBLANG_ENGLISH_US = 0x01


def MAKELANGID(primary, sublang):
    return (primary & 0xFF) | (sublang & 0xFF) << 16


LCID_ENGLISH = MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US)
LCID_DEFAULT = MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT)
LCID_NEUTRAL = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL)
assert LCID_NEUTRAL == 0

FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100
FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000
FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200


def FormatMessageSystem(id, langid=LCID_ENGLISH):
    sys_flag = (
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_IGNORE_INSERTS)

    bufptr = LPWSTR()

    chars = windll.kernel32.FormatMessageW(sys_flag, None, id, langid,
                                           byref(bufptr), 0, None)
    if chars == 0:
        chars = windll.kernel32.FormatMessageW(
                sys_flag, None, id, LCID_NEUTRAL, byref(bufptr), 0, None)
        if chars == 0:
            # XXX: You probably want to call GetLastError() here
            return "FormatMessageW failed"

    val = bufptr.value[:chars]
    windll.kernel32.LocalFree(bufptr)
    return val
