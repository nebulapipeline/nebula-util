from __future__ import print_function

from typing import Tuple
from types import FunctionType
import os
import re


class FileTexturePath(object):
    __tokens__ = {
            '<f>': r'\d+',
            '<udim>': r'1\d{3}',
            'u<u>_v<v>': r'u\d_v\d',
            '\\?+': lambda tt: r'\d{%d,}' % len(tt),
            '#+': lambda tt: r'\d{%d,}' % len(tt)
    }

    def __init__(self, path):
        # type: (str)
        path = os.path.normcase(path)
        if not os.path.isabs(path):
            path = os.path.abspath(path)
        self._path_parts = FileTexturePath.split_path(path)

    @classmethod
    def _split_path(cls, path):
        # type: (str) -> Tuple[str]
        dirname, basename = os.path.split(path)
        if not dirname or not basename:
            return (dirname or basename, )
        else:
            parts = cls._split_path(dirname)
            return parts + (basename, )

    @classmethod
    def split_path(cls, path):
        # type: (str) -> Tuple[str]
        '''Split path into parts'''
        parts = cls._split_path(path)
        parts, basename = parts[:-1], parts[-1]
        token = None
        for ttype, texp in cls.__tokens__.items():
            match = re.search(ttype, basename, re.I)
            if match:
                token = match.group()
                break
        if token:
            repl = texp(token) if isinstance(texp, FunctionType) else texp
            token = re.escape(token)
            basename = re.sub(
                    token,
                    repl,
                    basename)
        return parts + (basename, )

    @property
    def re(self):
        return (re.escape(self.dirname) + re.escape(os.sep) + self.basename
                + '$')

    @property
    def rec(self):
        return re.compile(self.re, re.IGNORECASE)

    @property
    def dirname(self):
        return os.path.join(*self._path_parts[:-1])

    @property
    def basename(self):
        return self._path_parts[-1]

    def matches(self, other):
        return bool(self.rec.match(other))

    def get_files(self):
        rec = re.compile(self.basename, re.IGNORECASE)
        return [filename for filename in os.listdir(self.dirname)
                if rec.match(filename)]

    def __repr__(self):
        return "%s(%r)" % (self.__class__.__name__, self.__str__())

    def __str__(self):
        return os.path.join(*self._path_parts)

    def __hash__(self):
        return hash(self._path_parts[:-1])

    def __eq__(self, other):
        if not isinstance(other, FileTexturePath):
            other = self.__class__(os.path.normpath(other))
        if str(self) == str(other):
            return True
        return self.matches(str(other))
