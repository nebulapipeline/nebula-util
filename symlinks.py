import re
import os
from collections import namedtuple


if os.name == 'nt':
    from _symlinks_nt import getSymlinks
else:
    from _symlinks_posix import getSymlinks

from _symlinks_common import symlinkMapping


def translateSymlink(path, maps=None):
    '''
    :type path: str
    :type maps: None or list of symlinkMapping
    '''
    path = os.path.normpath(path).lower()
    dirname = os.path.dirname(path)
    basename = os.path.basename(path)
    if maps is None:
        maps = getSymlinks(dirname)
    for m in maps:
        if m.location == dirname and m.name == basename:
            return m.target
    return path


def translatePath(path, maps=None, linkdir=None, reverse=False, single=True):
    '''
    :type path: str
    :type maps: None or list of symlinkMapping
    :type linkdir: None or str
    :type single: bool
    '''
    paths = []
    path = os.path.normpath(path.strip())
    if maps is None:
        if linkdir is not None and os.path.exists(linkdir):
            maps = getSymlinks(linkdir)
        else:
            raise ValueError('linkdir is invalid')

    for m in maps:
        linkpath = os.path.join(m.location, m.name)

        tofind, toreplace = linkpath, m.target
        if reverse:
            tofind, toreplace = toreplace, tofind

        tofind += '\\'
        toreplace += '\\'
        tofind = '^' + tofind.replace('\\', r'\\')
        toreplace = toreplace.replace('\\', r'\\')
        if re.search(tofind, path, re.IGNORECASE):
            newpath = re.sub(tofind, toreplace, path, 1, re.I)
            paths.append(newpath)

    if single:
        return paths[0] if paths else path
    else:
        return paths


def test():
    maps = getSymlinks(r'\\dbserver\assets')
    print translatePath(
        '\\\\dbserver\\assets\\captain_khalfan\\02_production'
        '\\ep09\\assets\\character\\captain_khalfan_regular\\rig'
        '\\captain_khalfan_regular_rig.ma',
        maps,
        single=False)


if __name__ == '__main__':
    test()
