from ..filetexturepath import FileTexturePath

import unittest


class TestFileTexturePath(unittest.TestCase):

    def test_sequence(self):
        sequence = FileTexturePath(r"d:\test\test.<f>.py")
        self.assertEqual(sequence, FileTexturePath(r"d:\test\test.0001.py"))
        self.assertEqual(sequence, FileTexturePath(r"d:\test\test.1.py"))

    def test_udim(self):
        udim = FileTexturePath(r"d:\test\test.<udim>.py")
        self.assertEqual(udim, FileTexturePath(r"d:\test\test.1001.py"))

    def test_udim2(self):
        udim = FileTexturePath(r"d:\test\test.u<u>_v<v>.exr")
        self.assertEqual(udim, FileTexturePath(r"d:\test\test.u1_v3.exr"))

    def test_question(self):
        question = FileTexturePath(r"d:\test\test.????.exr")
        self.assertEqual(question, FileTexturePath(r"d:\test\test.9866.exr"))

    def test_hash(self):
        hashes = FileTexturePath(r"d:\test\test.####.exr")
        self.assertEqual(hashes, FileTexturePath(r"d:\test\test.9866.exr"))

    def test_not_equal(self):
        ftp = FileTexturePath(r"d:\test\test.####.exr")
        self.assertNotEqual(ftp, r"d:\test\test.9880.exr\tst")

    def test_same(self):
        ftp = FileTexturePath(r"d:\test\test.exr")
        self.assertEqual(ftp, r"d:/test/test.exr")


if __name__ == "__main__":
    unittest.main()
